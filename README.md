# hyperf-rpc-demo
Hyperf RPC Demo - 基于Hyperf下json-RPC的Demo

### Step 1 - 安装并启动consul

- 安装consul，并设置全局可访问

- 新建一个consul目录，在consul目录下新建etc和data文件夹

启动命令：

```
cd /path/consul

consul agent -dev -ui -config-dir=./etc -data-dir=./data -client=0.0.0.0

或者执行
./start.sh


```

删除服务
http://127.0.0.1:8500/v1/agent/service/deregister/服务名称

示例：
http://127.0.0.1:8500/v1/agent/service/deregister/CalculatorService



服务注册方式有两种方式：
    服务启动+API注册
    consul根据配置文件启动注册

### Step 2 - 启动服务提供者
```
- 切换到hyperf-provider目录
- 执行composer install
- php bin/hyperf.php start


- 会自动注册服务到consul

```

回过神来思考一下，留意config配置文件夹内的
```
- autoload/consul.php
- autoload/server.php
```

前者配置了注册到consul的地址，后者配置了服务提供的端口和地址，是不是就和provider.json的配置对应上了，这样consul就能发现到服务提供者了

### Step 3 - 启动服务消费者

```
- 切换到hyperf-consumer目录
- 执行composer install
- php bin/hyperf.php start
```

访问：http://127.0.0.1:9503

### 注意

如果添加了服务的方法，需要清空一下runtime - container - proxy


# php-microservice-demo

一个docker的简单示例



# 参考来源
https://segmentfault.com/a/1190000022273733
https://segmentfault.com/a/1190000020421333
